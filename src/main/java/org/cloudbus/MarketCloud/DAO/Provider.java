package org.cloudbus.MarketCloud.DAO;

import javax.persistence.*;

@Entity
@Table(name="provider")
public class Provider {
	public Provider() {
		// TODO Auto-generated constructor stub
	}
	
	public Provider(String name, String accesskey, String secretkey) {
		super();
		this.name = name;
		this.accesskey = accesskey;
		this.secretkey = secretkey;
	}

	@Id
	@GeneratedValue
	private int id;
	
	private String name;

	private String accesskey;
	
	private String secretkey;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAccesskey() {
		return accesskey;
	}

	public void setAccesskey(String accesskey) {
		this.accesskey = accesskey;
	}

	
	public String getSecretkey() {
		return secretkey;
	}

	public void setSecretkey(String secretkey) {
		this.secretkey = secretkey;
	}
	
}
