package org.cloudbus.MarketCloud;

import java.util.Iterator;
import java.util.Properties;

import org.jclouds.ContextBuilder;
import org.jclouds.compute.ComputeService;
import org.jclouds.compute.ComputeServiceContext;
import org.jclouds.logging.slf4j.config.SLF4JLoggingModule;
import org.jclouds.sshj.config.SshjSshClientModule;

import com.google.common.collect.ImmutableSet;
import com.google.inject.Module;

public class CloudProviderFactory {
	private static Iterable<Module> modules = ImmutableSet.<Module> of(new SLF4JLoggingModule(),new SshjSshClientModule());

	//private final static String[] SUPPORTED_PROVIDERS = {"openstack-nova","amazon"};
	private static ComputeServiceContext getContext(String providerName, String url, String user, String pass){
		if(providerName.contains("openstack")){
			ComputeServiceContext context = ContextBuilder.newBuilder(providerName)
					.endpoint(/*"https://keystone.rc.nectar.org.au:5000/v2.0/"*/url)
					.credentials(user, pass)
					.modules(modules)
					.buildView(ComputeServiceContext.class);

			return context;
		}
		else{
			ComputeServiceContext context = ContextBuilder.newBuilder(providerName)
					//.endpoint(/*"https://keystone.rc.nectar.org.au:5000/v2.0/"*/url)
					.credentials(user, pass)
					.modules(modules)
					.buildView(ComputeServiceContext.class);

			return context;
		}
	}
	public static ComputeService getProviderInstance(String providerName,String user, String pass){
		String provider,url=null;
		//List<String> result = new ArrayList<String>();
		   Properties p = new Properties();
		    //  try {
				//p.load(AuctionController.class.getResourceAsStream("providers.properties"));
				Iterator<Object> iterator = p.keySet().iterator();
				while(iterator.hasNext()){
					String key = (String)iterator.next();
					if(key.endsWith(".name") ){
						provider = (String)p.get(key); 
						if(provider.equalsIgnoreCase(providerName)){
							url = (String) p.get(iterator.next());
							break;
						}
						//result.add(temp);
					}
						
				}
				//String[] result = new String[temp.size()];
				//return temp.toArray(result);
				System.out.println("url"+url);
				if(url != null){
					ComputeServiceContext context = getContext(providerName.toLowerCase(), url, user, pass);
					if(context != null)
						return context.getComputeService();
				}
				return null;
				
				
			
		
	}

}
