package org.cloudbus.auction;

public class Order {
		private int slots;
		private long bid;
		private int quantity;

		public Order(int slots, long bid, int quantity) {
			super();
			this.slots = slots;
			this.bid = bid;
			this.quantity = quantity;
		}

		public int getSlots() {
			return slots;
		}

		public long getBidPrice() {
			return bid;
		}

		public int getQuantity() {
			return quantity;
		}
		
	
		public void setBidPrice(long bid) {
			this.bid = bid;
		}

		public void setSlots(int slots) {
			this.slots = slots;
		}

		public void setQuantity(int quantity) {
			this.quantity = quantity;
		}
		
		
		@Override
		public boolean equals(Object o){
			if(o instanceof Order)
				if(this.getQuantity() == ((Order)o).getQuantity() && this.getSlots() == ((Order)o).getSlots() && this.getBidPrice() == ((Order)o).getBidPrice())
					return true;
				else 
					return false;
			else
				return false;
		}
		
		
		@Override 
		public String toString() {
			StringBuilder result = new StringBuilder();
			String NEW_LINE = System.getProperty("line.separator");

			result.append(this.getClass().getName() + " Object {" + NEW_LINE);
			result.append(" slots: " + slots + NEW_LINE);
			result.append(" bid: " + bid + NEW_LINE);
			result.append(" quantity: " + quantity + NEW_LINE );
			result.append("}");

			return result.toString();
		}

	}
