package org.cloudbus.MarketCloud.DAO;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "prices")
public class Prices {
	public Prices() {
		// TODO Auto-generated constructor stub
	}

	public Prices(int provider, int vmtype, Date time, double price,
			double optimalprice) {
		super();
		this.provider = provider;
		this.type = vmtype;
		this.timestamp = time;
		this.price = price;
		this.optimalprice = optimalprice;
	}

	@Id
	@GeneratedValue
	private int id;

	private int provider;

	@Column(name = "vmtype")
	private int type;

	@Column(name = "time")
	private Date timestamp;

	private double price;

	private double optimalprice;

	public int getId() {
		return id;
	}

	public int getProvider() {
		return provider;
	}

	public void setProvider(int provider) {
		this.provider = provider;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public double getOptimalprice() {
		return optimalprice;
	}

	public void setOptimalprice(double optimalprice) {
		this.optimalprice = optimalprice;
	}

	@Override
	public String toString() {
		StringBuilder result = new StringBuilder();
		String NEW_LINE = System.getProperty("line.separator");

		result.append(this.getClass().getName() + " Object {" + NEW_LINE);
		result.append(" id: " + id + NEW_LINE);
		result.append(" provider: " + provider + NEW_LINE);
		result.append(" vmtype: " + type + NEW_LINE);
		result.append(" price: " + price + NEW_LINE);
		result.append(" optimal price: " + optimalprice + NEW_LINE);
		result.append(" time: " + timestamp + NEW_LINE);
		result.append("}");

		return result.toString();
	}

	@Override
	public boolean equals(Object o) {
		if (o instanceof Prices)
			if (this.getProvider() == ((Prices) o).getProvider() &&
				this.getType() == ((Prices) o).getType() &&
				(Math.abs(this.getPrice()- (((Prices) o).getPrice()))< 0.0000001) &&
				(Math.abs(this.getOptimalprice()- (((Prices) o).getOptimalprice()))< 0.000001)
				)
				return true;
			else
				return false;
		else
			return false;
	}

}
