package org.cloudbus.MarketCloud.DAO;

import java.util.Date;

import javax.persistence.*;

@Entity
@Table(name="availables")
public class Availables {
	public Availables() {
		// TODO Auto-generated constructor stub
	}
	
	public Availables(int provider, int type, short amount, Date timestamp) {
		super();
		this.provider = provider;
		this.type = type;
		this.amount = amount;
		this.timestamp = timestamp;
	}

	@Id
	//@GeneratedValue
	private int id;
	
	private int provider;
	
	@Column(name="vmtype")
	private int type ;
	
	private short amount;
	
	@Column(name="time")
	private Date timestamp;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getProvider() {
		return provider;
	}

	public void setProvider(int provider) {
		this.provider = provider;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public short getAmount() {
		return amount;
	}

	public void setAmount(short amount) {
		this.amount = amount;
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}
	
	
}
