package org.cloudbus.MarketCloud.DAO;

import javax.persistence.*;

@Entity
@Table(name="vmtypes")
public class VMType {
	public VMType() {
		// TODO Auto-generated constructor stub
	}
	
    public VMType(int provider, String type) {
		super();
		this.provider = provider;
		this.type = type;
	}

	@Id
    @GeneratedValue
	private int id;
    
    private int provider;
	
	private String type;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getProvider() {
		return provider;
	}

	public void setProvider(int provider) {
		this.provider = provider;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	
	
}
