package org.cloudbus.MarketCloud;

import org.jclouds.compute.ComputeService;

public abstract class Auctions {
	
	public abstract void runAuction(String provider, String VmType, String username, ComputeService cs);
	
    public static Auctions getAuction(String provider, String VmType){
    	if(provider.equalsIgnoreCase("openstack-nova") && VmType.equalsIgnoreCase("m1.small"))
    		return new NovaAuctionImplXsmall();
    	return null;
    }
}
