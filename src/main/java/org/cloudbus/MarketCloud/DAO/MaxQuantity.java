package org.cloudbus.MarketCloud.DAO;

import java.util.Date;

import javax.persistence.*;

@Entity
@Table(name="maxqty")
public class MaxQuantity {
	public MaxQuantity() {
		// TODO Auto-generated constructor stub
	}
	
	public MaxQuantity(int provider, int type, short quantity, Date timestamp) {
		super();
		this.provider = provider;
		this.type = type;
		this.quantity = quantity;
		this.timestamp = timestamp;
	}

	@Id
	//@GeneratedValue
	private int id;
	
	private int provider;
	
	@Column(name="vmtype")
	private int type ;
	
	private short quantity;
	
	@Column(name="time")
	private Date timestamp;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getProvider() {
		return provider;
	}

	public void setProvider(int provider) {
		this.provider = provider;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public short getQuantity() {
		return quantity;
	}

	public void setAmount(short quantity) {
		this.quantity = quantity;
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}
	
	
}
