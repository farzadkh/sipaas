-- phpMyAdmin SQL Dump
-- version 4.2.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 02, 2014 at 04:10 PM
-- Server version: 5.1.73-0ubuntu0.10.04.1
-- PHP Version: 5.3.2-1ubuntu4.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `auctionDB`
--

-- --------------------------------------------------------

--
-- Table structure for table `availables`
--

CREATE TABLE IF NOT EXISTS `availables` (
`id` int(11) NOT NULL,
  `provider` int(11) NOT NULL,
  `vmtype` int(11) NOT NULL,
  `amount` smallint(6) NOT NULL,
  `time` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=45 ;

--
-- Dumping data for table `availables`
--

INSERT INTO `availables` (`id`, `provider`, `vmtype`, `amount`, `time`) VALUES
(3, 18, 6, 20, '2014-05-30 14:37:55'),
(4, 18, 6, 20, '2014-06-01 15:47:20'),
(5, 18, 6, 100, '2014-06-01 15:47:39'),
(6, 18, 6, 5, '2014-06-01 17:20:52'),
(7, 18, 6, 12, '2014-06-01 17:21:16'),
(8, 18, 6, 12, '2014-06-01 17:21:27'),
(9, 18, 6, 100, '2014-06-01 17:21:43'),
(10, 18, 6, 100, '2014-06-01 17:41:45'),
(11, 18, 6, 100, '2014-06-01 17:43:50'),
(12, 18, 6, 100, '2014-06-01 17:44:27'),
(13, 18, 6, 100, '2014-06-01 17:44:31'),
(14, 18, 6, 100, '2014-06-01 17:46:11'),
(15, 18, 6, 100, '2014-06-01 17:48:16'),
(16, 18, 6, 5, '2014-06-01 17:48:29'),
(17, 18, 6, 5, '2014-06-01 17:48:34'),
(18, 18, 6, 100, '2014-06-01 17:48:41'),
(19, 18, 6, 10, '2014-06-01 17:48:55'),
(20, 18, 6, 10, '2014-06-01 17:48:57'),
(21, 18, 6, 5, '2014-06-01 17:49:04'),
(22, 18, 6, 5, '2014-06-01 17:49:05'),
(23, 18, 6, 12, '2014-06-01 17:49:10'),
(24, 18, 6, 12, '2014-06-01 17:49:14'),
(25, 18, 6, 14, '2014-06-01 17:49:19'),
(26, 18, 6, 15, '2014-06-01 17:49:25'),
(27, 18, 6, 15, '2014-06-01 17:49:28'),
(28, 18, 6, 5, '2014-06-01 17:49:31'),
(29, 18, 6, 19, '2014-06-01 17:49:37'),
(30, 18, 6, 20, '2014-06-01 17:49:40'),
(31, 18, 6, 30, '2014-06-01 17:49:44'),
(32, 18, 6, 15, '2014-06-01 17:50:01'),
(33, 18, 6, 30, '2014-06-01 17:50:06'),
(34, 18, 6, 30, '2014-06-01 18:17:19'),
(35, 18, 6, 100, '2014-06-01 18:17:28'),
(36, 18, 6, 100, '2014-06-01 18:18:47'),
(37, 18, 6, 5, '2014-06-01 18:18:54'),
(38, 18, 6, 100, '2014-06-01 18:18:59'),
(39, 18, 6, 1000, '2014-06-01 18:19:10'),
(40, 18, 6, 10000, '2014-06-01 18:19:14'),
(41, 18, 6, 5, '2014-06-01 18:19:20'),
(42, 18, 6, 100, '2014-06-01 18:19:24'),
(43, 18, 6, 4, '2014-06-01 18:20:24'),
(44, 18, 6, 100, '2014-06-01 18:22:54');

-- --------------------------------------------------------

--
-- Table structure for table `bids`
--

CREATE TABLE IF NOT EXISTS `bids` (
`id` int(11) NOT NULL,
  `provider` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `qty` smallint(6) NOT NULL,
  `bid` double NOT NULL,
  `bidref` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=29 ;

--
-- Dumping data for table `bids`
--

INSERT INTO `bids` (`id`, `provider`, `type`, `qty`, `bid`, `bidref`) VALUES
(14, 18, 6, 1, 0.06, 1),
(15, 18, 6, 5, 0.03, 14),
(16, 18, 6, 2, 0.015, 12),
(17, 18, 6, 3, 0.01, 13),
(18, 18, 6, 3, 0.08, 10),
(19, 18, 6, 10, 0.09, 11),
(24, 18, 6, 10, 0.1, 15),
(28, 18, 6, 1, 0.11, 16);

-- --------------------------------------------------------

--
-- Table structure for table `prices`
--

CREATE TABLE IF NOT EXISTS `prices` (
`id` int(11) NOT NULL,
  `provider` int(11) NOT NULL,
  `vmtype` int(11) NOT NULL,
  `time` datetime NOT NULL,
  `price` double NOT NULL,
  `optimalprice` double NOT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=50 ;

--
-- Dumping data for table `prices`
--

INSERT INTO `prices` (`id`, `provider`, `vmtype`, `time`, `price`, `optimalprice`) VALUES
(10, 18, 6, '2014-06-01 17:43:11', 0.029, 0.08),
(11, 18, 6, '2014-06-01 17:48:29', 0.1, 0.1),
(12, 18, 6, '2014-06-01 17:48:41', 0.029, 0.08),
(13, 18, 6, '2014-06-01 17:48:55', 0.1, 0.1),
(14, 18, 6, '2014-06-01 17:49:19', 0.09, 0.09),
(15, 18, 6, '2014-06-01 17:49:31', 0.1, 0.1),
(16, 18, 6, '2014-06-01 17:49:37', 0.09, 0.09),
(17, 18, 6, '2014-06-01 17:49:44', 0.029, 0.08),
(18, 18, 6, '2014-06-01 17:50:01', 0.09, 0.09),
(19, 18, 6, '2014-06-01 17:50:07', 0.029, 0.08),
(20, 18, 6, '2014-06-01 17:50:36', 0.1, 0.08),
(21, 18, 6, '2014-06-01 18:01:19', 0.2, 0),
(22, 18, 6, '2014-06-01 18:01:23', 0.1, 0.1),
(23, 18, 6, '2014-06-01 18:01:27', 0.015, 0.08),
(24, 18, 6, '2014-06-01 18:01:44', 0.02, 0.08),
(25, 18, 6, '2014-06-01 18:01:47', 0.03, 0.08),
(26, 18, 6, '2014-06-01 18:01:50', 0.04, 0.08),
(27, 18, 6, '2014-06-01 18:01:52', 0.05, 0.08),
(28, 18, 6, '2014-06-01 18:01:55', 0.06, 0.08),
(29, 18, 6, '2014-06-01 18:01:57', 0.07, 0.08),
(30, 18, 6, '2014-06-01 18:02:00', 0.2, 0),
(31, 18, 6, '2014-06-01 18:02:03', 0.1, 0.1),
(32, 18, 6, '2014-06-01 18:02:41', 0.015, 0.08),
(33, 18, 6, '2014-06-01 18:03:15', 0.1, 0.1),
(34, 18, 6, '2014-06-01 18:03:38', 0.015, 0.08),
(35, 18, 6, '2014-06-01 18:11:37', 0.05, 0.08),
(36, 18, 6, '2014-06-01 18:14:22', 0.06, 0.08),
(37, 18, 6, '2014-06-01 18:14:55', 0.07, 0.08),
(38, 18, 6, '2014-06-01 18:15:38', 0.09, 0.09),
(39, 18, 6, '2014-06-01 18:15:56', 0.015, 0.08),
(40, 18, 6, '2014-06-01 18:16:03', 0.03, 0.08),
(41, 18, 6, '2014-06-01 18:16:06', 0.09, 0.09),
(42, 18, 6, '2014-06-01 18:16:09', 0.015, 0.08),
(43, 18, 6, '2014-06-01 18:18:54', 0.1, 0.1),
(44, 18, 6, '2014-06-01 18:18:59', 2.5e-05, 0.08),
(45, 18, 6, '2014-06-01 18:19:10', 0, 0.08),
(46, 18, 6, '2014-06-01 18:19:21', 0.1, 0.1),
(47, 18, 6, '2014-06-01 18:19:24', 7.5e-05, 0.08),
(48, 18, 6, '2014-06-01 18:22:31', 0.1, 0.1),
(49, 18, 6, '2014-06-01 18:23:51', 5.1e-05, 0.08);

-- --------------------------------------------------------

--
-- Table structure for table `provider`
--

CREATE TABLE IF NOT EXISTS `provider` (
`id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `accesskey` varchar(36) NOT NULL,
  `secretkey` varchar(36) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21 ;

--
-- Dumping data for table `provider`
--

INSERT INTO `provider` (`id`, `name`, `accesskey`, `secretkey`) VALUES
(18, 'amazon', '46bb969d-9976-49b8-bb76-7b6940007768', '99934657-8dae-4ad1-b071-ff19b24985de'),
(20, 'amazon', '2d8d707c-f6d0-43e2-b509-68a760decc47', '7887a32e-1476-4e2d-96c9-c963dee930b5');

-- --------------------------------------------------------

--
-- Table structure for table `reserveprice`
--

CREATE TABLE IF NOT EXISTS `reserveprice` (
`id` int(11) NOT NULL,
  `provider` int(11) NOT NULL,
  `vmtype` int(11) NOT NULL,
  `price` double NOT NULL,
  `time` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=68 ;

--
-- Dumping data for table `reserveprice`
--

INSERT INTO `reserveprice` (`id`, `provider`, `vmtype`, `price`, `time`) VALUES
(3, 18, 6, 0.02, '2014-05-30 13:39:46'),
(4, 18, 6, 0.08, '2014-05-30 13:40:03'),
(7, 18, 6, 0.01, '2014-05-30 14:31:43'),
(8, 18, 6, 0.01, '2014-05-30 14:36:52'),
(9, 18, 6, 0.01, '2014-06-01 17:22:11'),
(10, 18, 6, 0.2, '2014-06-01 17:22:19'),
(11, 18, 6, 0.018, '2014-06-01 17:22:31'),
(12, 18, 6, 0.014, '2014-06-01 17:23:45'),
(13, 18, 6, 0.022, '2014-06-01 17:23:50'),
(14, 18, 6, 0.025, '2014-06-01 17:23:58'),
(15, 18, 6, 0.029, '2014-06-01 17:24:02'),
(16, 18, 6, 0.029, '2014-06-01 17:24:41'),
(17, 18, 6, 0.029, '2014-06-01 17:30:47'),
(18, 18, 6, 0.029, '2014-06-01 17:50:28'),
(19, 18, 6, 0.1, '2014-06-01 17:50:36'),
(20, 18, 6, 0.029, '2014-06-01 17:50:40'),
(21, 18, 6, 0.029, '2014-06-01 17:50:41'),
(22, 18, 6, 0.029, '2014-06-01 18:01:08'),
(23, 18, 6, 0.2, '2014-06-01 18:01:18'),
(24, 18, 6, 0.1, '2014-06-01 18:01:23'),
(25, 18, 6, 0.015, '2014-06-01 18:01:27'),
(26, 18, 6, 0.002, '2014-06-01 18:01:32'),
(27, 18, 6, 0.0001, '2014-06-01 18:01:37'),
(28, 18, 6, 0.01, '2014-06-01 18:01:42'),
(29, 18, 6, 0.02, '2014-06-01 18:01:44'),
(30, 18, 6, 0.03, '2014-06-01 18:01:47'),
(31, 18, 6, 0.04, '2014-06-01 18:01:50'),
(32, 18, 6, 0.05, '2014-06-01 18:01:52'),
(33, 18, 6, 0.06, '2014-06-01 18:01:54'),
(34, 18, 6, 0.07, '2014-06-01 18:01:57'),
(35, 18, 6, 0.2, '2014-06-01 18:01:59'),
(36, 18, 6, 0.2, '2014-06-01 18:02:02'),
(37, 18, 6, 0.1, '2014-06-01 18:02:03'),
(38, 18, 6, 0.01, '2014-06-01 18:02:41'),
(39, 18, 6, 0.001, '2014-06-01 18:02:44'),
(40, 18, 6, 0.0002, '2014-06-01 18:02:52'),
(41, 18, 6, 0.01, '2014-06-01 18:02:59'),
(42, 18, 6, 0.1, '2014-06-01 18:03:14'),
(43, 18, 6, 0.1, '2014-06-01 18:03:19'),
(44, 18, 6, 0.01, '2014-06-01 18:03:38'),
(45, 18, 6, 0.01, '2014-06-01 18:05:06'),
(46, 18, 6, 0.05, '2014-06-01 18:06:22'),
(47, 18, 6, 0.05, '2014-06-01 18:08:26'),
(48, 18, 6, 0.05, '2014-06-01 18:10:48'),
(49, 18, 6, 0.05, '2014-06-01 18:11:57'),
(50, 18, 6, 0.05, '2014-06-01 18:13:08'),
(51, 18, 6, 0.05, '2014-06-01 18:13:43'),
(52, 18, 6, 0.06, '2014-06-01 18:14:05'),
(53, 18, 6, 0.06, '2014-06-01 18:14:47'),
(54, 18, 6, 0.07, '2014-06-01 18:14:55'),
(55, 18, 6, 0.06, '2014-06-01 18:15:06'),
(56, 18, 6, 0.05, '2014-06-01 18:15:26'),
(57, 18, 6, 0.04, '2014-06-01 18:15:29'),
(58, 18, 6, 0.09, '2014-06-01 18:15:38'),
(59, 18, 6, 0.09, '2014-06-01 18:15:48'),
(60, 18, 6, 0.09, '2014-06-01 18:15:53'),
(61, 18, 6, 0.01, '2014-06-01 18:15:56'),
(62, 18, 6, 0.01, '2014-06-01 18:15:59'),
(63, 18, 6, 0.03, '2014-06-01 18:16:03'),
(64, 18, 6, 0.09, '2014-06-01 18:16:06'),
(65, 18, 6, 0.01, '2014-06-01 18:16:09'),
(66, 18, 6, 0.001, '2014-06-01 18:16:11'),
(67, 18, 6, 0.0001, '2014-06-01 18:16:58');

-- --------------------------------------------------------

--
-- Table structure for table `vmtypes`
--

CREATE TABLE IF NOT EXISTS `vmtypes` (
`id` int(11) NOT NULL,
  `provider` int(11) NOT NULL,
  `type` varchar(30) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `vmtypes`
--

INSERT INTO `vmtypes` (`id`, `provider`, `type`) VALUES
(6, 18, 'm1.small');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `availables`
--
ALTER TABLE `availables`
 ADD PRIMARY KEY (`id`), ADD KEY `provider` (`provider`), ADD KEY `vmtype` (`vmtype`);

--
-- Indexes for table `bids`
--
ALTER TABLE `bids`
 ADD PRIMARY KEY (`id`), ADD KEY `provider` (`provider`,`type`), ADD KEY `type` (`type`);

--
-- Indexes for table `prices`
--
ALTER TABLE `prices`
 ADD PRIMARY KEY (`id`), ADD KEY `provider` (`provider`), ADD KEY `vmtype` (`vmtype`);

--
-- Indexes for table `provider`
--
ALTER TABLE `provider`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reserveprice`
--
ALTER TABLE `reserveprice`
 ADD PRIMARY KEY (`id`), ADD KEY `provider` (`provider`), ADD KEY `vmtype` (`vmtype`);

--
-- Indexes for table `vmtypes`
--
ALTER TABLE `vmtypes`
 ADD PRIMARY KEY (`id`), ADD KEY `provider` (`provider`), ADD KEY `provider_2` (`provider`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `availables`
--
ALTER TABLE `availables`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=45;
--
-- AUTO_INCREMENT for table `bids`
--
ALTER TABLE `bids`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `prices`
--
ALTER TABLE `prices`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=50;
--
-- AUTO_INCREMENT for table `provider`
--
ALTER TABLE `provider`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `reserveprice`
--
ALTER TABLE `reserveprice`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=68;
--
-- AUTO_INCREMENT for table `vmtypes`
--
ALTER TABLE `vmtypes`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `availables`
--
ALTER TABLE `availables`
ADD CONSTRAINT `provider_available` FOREIGN KEY (`provider`) REFERENCES `provider` (`id`) ON UPDATE CASCADE,
ADD CONSTRAINT `vmtype_available` FOREIGN KEY (`vmtype`) REFERENCES `vmtypes` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `bids`
--
ALTER TABLE `bids`
ADD CONSTRAINT `bids_provider` FOREIGN KEY (`provider`) REFERENCES `provider` (`id`) ON UPDATE CASCADE,
ADD CONSTRAINT `bids_vmtype` FOREIGN KEY (`type`) REFERENCES `vmtypes` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `reserveprice`
--
ALTER TABLE `reserveprice`
ADD CONSTRAINT `reserve_provider` FOREIGN KEY (`provider`) REFERENCES `provider` (`id`) ON UPDATE CASCADE,
ADD CONSTRAINT `reserve_vm` FOREIGN KEY (`vmtype`) REFERENCES `vmtypes` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `vmtypes`
--
ALTER TABLE `vmtypes`
ADD CONSTRAINT `provider_vm` FOREIGN KEY (`provider`) REFERENCES `provider` (`id`) ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
