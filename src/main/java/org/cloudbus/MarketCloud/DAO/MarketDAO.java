package org.cloudbus.MarketCloud.DAO;



import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;


@Repository
public class MarketDAO {
	private Logger logger = Logger.getLogger("MarketDAO");
	
	@Autowired
	private SessionFactory sessionFactory; 
	
	@SuppressWarnings("unchecked")
	@Transactional
	public List<Bids> listBids(int provider,int type){
		
		
		Session session = sessionFactory.getCurrentSession();
		Query q = session.createQuery("FROM Bids WHERE id= :provider AND type= :type");
		q.setParameter("provider", provider);
		q.setParameter("type", type);
		return q.list();
	}
	@Transactional
	public void addProvider(Provider provider){
		//logger.debug("adding provider");
		if(sessionFactory == null)
			System.out.println("sessionfactory is null");
		sessionFactory.getCurrentSession().save(provider);
	}
	@Transactional
	public void removeProvider(Provider provider){
		Session session = sessionFactory.getCurrentSession();
		if(sessionFactory == null)
			System.out.println("sessionfactory is null");


		Query query = session.createQuery("DELETE ReservePrice WHERE provider = :id");
		query.setParameter("id", provider.getId());
		query.executeUpdate();

		query = session.createQuery("DELETE Availables WHERE provider = :id");
		query.setParameter("id", provider.getId());
		query.executeUpdate();
				
		query = session.createQuery("DELETE Prices WHERE provider = :id");
		query.setParameter("id", provider.getId());
		query.executeUpdate();
		
		query = session.createQuery("DELETE Bids WHERE provider = :id");
		query.setParameter("id", provider.getId());
		query.executeUpdate();
		
		//logger.debug("adding provider");
		sessionFactory.getCurrentSession().delete(provider);
	}
	@Transactional
    public void addVmType(VMType vm){
    	//logger.debug("adding provider");
    	sessionFactory.getCurrentSession().save(vm);
    }
	
	@Transactional
    public void removeVmType(VMType vm){
    	//logger.debug("adding provider");
		Session session = sessionFactory.getCurrentSession();

		Query query = session.createQuery("DELETE ReservePrice WHERE type = :id");
		query.setParameter("id", vm.getId());
		query.executeUpdate();

		query = session.createQuery("DELETE Availables WHERE type = :id");
		query.setParameter("id", vm.getId());
		query.executeUpdate();
				
		query = session.createQuery("DELETE Prices WHERE type = :id");
		query.setParameter("id", vm.getId());
		query.executeUpdate();
		
		query = session.createQuery("DELETE Bids WHERE type = :id");
		query.setParameter("id", vm.getId());
		query.executeUpdate();
		
    	session.delete(vm);
    }
	
	@Transactional
    public void addAvailable(Availables av){
    	//logger.debug("adding provider");
    	sessionFactory.getCurrentSession().save(av);
    }
	@Transactional
    public void addReservePrice(ReservePrice rp){
    	//logger.debug("adding provider");
    	sessionFactory.getCurrentSession().save(rp);
    }
	@SuppressWarnings("unchecked")
	@Transactional
    public List<Provider> getAllProviders() {
    	  //logger.debug("Retrieving all providers");
    	   
    	  // Retrieve session from Hibernate
    	  Session session = sessionFactory.getCurrentSession();
    	   
    	  // Create a Hibernate query (HQL)
    	  Query query = session.createQuery("FROM  Provider");
    	   
    	  // Retrieve all
    	  return  query.list();
    }
	@SuppressWarnings("unchecked")
	@Transactional
    public List<VMType> getAllVMs() {
  	  logger.debug("Retrieving all VMs");
  	   
  	  // Retrieve session from Hibernate
  	  Session session = sessionFactory.getCurrentSession();
  	   
  	  // Create a Hibernate query (HQL)
  	  Query query = session.createQuery("FROM  VMType");
  	   
  	  // Retrieve all
  	  return  query.list();
  	 }
	@SuppressWarnings("unchecked")
	@Transactional
    public List<VMType> getAllVMs(int provider_id) {
  	  logger.debug("Retrieving all VMs");
  	   
  	  // Retrieve session from Hibernate
  	  Session session = sessionFactory.getCurrentSession();
  	   
  	  // Create a Hibernate query (HQL)
  	  Query query = session.createQuery("FROM  VMType v WHERE v.provider = :provider_id");
	  query.setParameter("provider_id", provider_id);
  	   
  	  // Retrieve all
  	  return  query.list();
  	 }
  	 
	@Transactional
	public Provider getProvider(Integer id){
		Session session = sessionFactory.getCurrentSession();
		Object result = session.get(Provider.class, id);
		if(result != null)
			return (Provider) result;
		return null;
	}
	@Transactional
	public Provider getProvider(String accesskey, String secretkey){
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery("FROM  Provider p WHERE p.accesskey= :provider_accesskey AND p.secretkey= :provider_secretkey");
		query.setParameter("provider_accesskey", accesskey);
		query.setParameter("provider_secretkey", secretkey);
		@SuppressWarnings("rawtypes")
		List result = query.list();
		if(result !=null && result.size()>0)
			return (Provider) result.get(0);
		return null;
	}
	
	@Transactional
	public VMType getVmType(String type, int provider_id){
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery("FROM  VMType v WHERE v.type= :vmtype AND v.provider= :provider_id");
		query.setParameter("vmtype", type);
		query.setParameter("provider_id", provider_id);
		@SuppressWarnings("rawtypes")
		List result = query.list();
		if(result !=null && result.size()>0)
			return (VMType) result.get(0);
		return null;
	}
		
	@Transactional
	public void addBid(Bids bid){
		sessionFactory.getCurrentSession().save(bid);
	}
	
	@Transactional
	public void removeBid(Bids bid){
		sessionFactory.getCurrentSession().delete(bid);
	}

	@Transactional
	public void removeBid(int provider, int ref){
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery("DELETE Bids where provider = :provider_id AND bidref = :bid_ref");
		query.setParameter("provider_id", provider);
		query.setParameter("bid_ref", ref);
		query.executeUpdate();
	}

	
	@Transactional
	public void uppdateBid(Bids bid, short qty){
	 Session session = sessionFactory.getCurrentSession();
     bid.setQty(qty);
	 session.update(bid);
	}
	
	@Transactional
	public Bids getBid(int provider, String ref){
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery("FROM  Bids b WHERE b.provider= :provider_id AND b.bidref= :bid_ref");
		query.setParameter("provider_id", provider);
		query.setParameter("bid_ref", ref);
		@SuppressWarnings("rawtypes")
		List result = query.list();
		if(result !=null && result.size()>0)
			return (Bids) result.get(0);
		return null;
	}
	
	@Transactional
	public double getRecentReservePrice(int provider, int vmtype) {
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery("FROM  ReservePrice r WHERE r.provider= :provider_id AND r.type= :vm_type ORDER BY r.timestamp DESC");
		query.setParameter("provider_id", provider);
		query.setParameter("vm_type", vmtype);
		@SuppressWarnings("rawtypes")
		List result = query.list();
		if(result !=null && result.size()>0)
			return ((ReservePrice) result.get(0)).getPrice();
		return 0;
	}
	
	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}
		
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	@Transactional
	public int getRecentSupply(int provider, int vmtype) {
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery("FROM  Availables a WHERE a.provider= :provider_id AND a.type= :vm_type ORDER BY a.timestamp DESC");
		query.setParameter("provider_id", provider);
		query.setParameter("vm_type", vmtype);
		@SuppressWarnings("rawtypes")
		List result = query.list();
		if(result !=null && result.size()>0)
			return ((Availables) result.get(0)).getAmount();
		return Integer.MAX_VALUE;
	}
	
	@SuppressWarnings("unchecked")
	@Transactional
	public List<Bids> getAllBids(int provider, int vmtype) {
	  	  logger.debug("Retrieving all VMs");
	  	  Session session = sessionFactory.getCurrentSession();
	  	  Query query = session.createQuery("FROM Bids b WHERE b.provider= :provider_id AND b.type= :vm_type ORDER BY b.bid DESC");
	  	  query.setParameter("provider_id", provider);
		  query.setParameter("vm_type", vmtype);
	  	  return  query.list();
	}
	
	@Transactional
	public void addPrice(Prices  price) {
		Prices lastprice = getPrice(price.getProvider(),price.getType());
		if(!price.equals(lastprice))
			sessionFactory.getCurrentSession().save(price);
	}
	
	@Transactional
	public Prices getPrice(int provider, int vmtype) {
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery("FROM  Prices p WHERE p.provider= :provider_id AND p.type= :vm_type ORDER BY p.timestamp DESC");
		query.setParameter("provider_id", provider);
		query.setParameter("vm_type", vmtype);
		@SuppressWarnings("rawtypes")
		List result = query.list();
		if(result !=null && result.size()>0)
			return ((Prices) result.get(0));
		return null;
	}
	
	@SuppressWarnings("unchecked")
	@Transactional
	public List<Prices> getAllPrices(int provider, int vmtype, Date from, Date to) {
	  	  logger.debug("Retrieving all Prices");
	  	  Session session = sessionFactory.getCurrentSession();
	  	  Query query = session.createQuery("FROM Prices p WHERE p.provider= :provider_id AND p.type= :vm_type "
	  	  		+ "AND p.timestamp <= :to AND p.timestamp >= :from ORDER BY p.timestamp ASC");
	  	  query.setParameter("provider_id", provider);
		  query.setParameter("vm_type", vmtype);
		  query.setParameter("from", from);
		  query.setParameter("to", to);

	  	  return  query.list();
	}
	
	@Transactional
	public void addMaxQty(MaxQuantity maxQuantity) {
    	sessionFactory.getCurrentSession().save(maxQuantity);
	}
	
	@Transactional
	public void addMaxPrice(MaxPrice maxPrice) {
    	sessionFactory.getCurrentSession().save(maxPrice);
	}
	
	@Transactional
	public double getRecentMaxPrice(int provider, int vmtype) {
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery("FROM  MaxPrice r WHERE r.provider= :provider_id AND r.type= :vm_type ORDER BY r.timestamp DESC");
		query.setParameter("provider_id", provider);
		query.setParameter("vm_type", vmtype);
		@SuppressWarnings("rawtypes")
		List result = query.list();
		if(result !=null && result.size()>0)
			return ((MaxPrice) result.get(0)).getPrice();
		return Double.MAX_VALUE;
	}
	
	@Transactional
	public int getRecentMaxQty(int provider, int vmtype) {
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery("FROM  MaxQuantity a WHERE a.provider= :provider_id AND a.type= :vm_type ORDER BY a.timestamp DESC");
		query.setParameter("provider_id", provider);
		query.setParameter("vm_type", vmtype);
		@SuppressWarnings("rawtypes")
		List result = query.list();
		if(result !=null && result.size()>0)
			return ((MaxQuantity) result.get(0)).getQuantity();
		return Integer.MAX_VALUE;
	}

}
