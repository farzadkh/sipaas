package org.cloudbus.MarketCloud;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.cloudbus.MarketCloud.DAO.Availables;
import org.cloudbus.MarketCloud.DAO.Bids;
import org.cloudbus.MarketCloud.DAO.MarketDAO;
import org.cloudbus.MarketCloud.DAO.MaxPrice;
import org.cloudbus.MarketCloud.DAO.MaxQuantity;
import org.cloudbus.MarketCloud.DAO.Prices;
import org.cloudbus.MarketCloud.DAO.Provider;
import org.cloudbus.MarketCloud.DAO.ReservePrice;
import org.cloudbus.MarketCloud.DAO.VMType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/services")
public class ProviderController {
	private String status_successfull = "{\"status\":\"success\"}";
	private String status_failure = "{\"status\":\"fail\"}";

	// private Logger logger = Logger.getLogger("AuctionController");

	@Autowired
	private MarketDAO marketDAO;

	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public @ResponseBody
	String registerProvider(@RequestParam(value = "name") String name) {
		try {

			UUID accesskey = UUID.randomUUID();
			UUID secretkey = UUID.randomUUID();

			Provider p = new Provider(name, accesskey.toString(),
					secretkey.toString());
			marketDAO.addProvider(p);

			String output = "{\"accesskey\":\"" + p.getAccesskey()
					+ "\",\"secretkey\":\"" + p.getSecretkey() + "\"}";

			return output;
		} catch (Exception e) {
			// logger.debug(e);
			e.printStackTrace();
			return status_failure;
		}
	}

	@RequestMapping(value = "/unregister", method = RequestMethod.POST)
	public @ResponseBody
	String unregisterProvider(
			@RequestParam(value = "accesskey") String accesskey,
			@RequestParam(value = "secretkey") String secretkey) {
		try {

			Provider p = marketDAO.getProvider(accesskey, secretkey);
			if (p == null)
				return "Provider is not registered!";
			
			marketDAO.removeProvider(p);

			return status_successfull;
		} catch (Exception e) {
			// logger.debug(e);
			e.printStackTrace();
			return status_failure;
		}
	}

	@RequestMapping(value = "/regvmtype", method = RequestMethod.POST)
	public @ResponseBody
	String regVmType(@RequestParam(value = "accesskey") String accesskey,
			@RequestParam(value = "secretkey") String secretkey,
			@RequestParam(value = "type") String type) {
		try {

			Provider p = marketDAO.getProvider(accesskey, secretkey);


			marketDAO.addVmType(new VMType(p.getId(), type));
			return status_successfull;
		} catch (Exception e) {
			// logger.debug(e);
			e.printStackTrace();
			return status_failure;
		}
	}

	@RequestMapping(value = "/unregvmtype", method = RequestMethod.POST)
	public @ResponseBody
	String unRegVmType(@RequestParam(value = "accesskey") String accesskey,
			@RequestParam(value = "secretkey") String secretkey,
			@RequestParam(value = "type") String type) {
		try {

			Provider p = marketDAO.getProvider(accesskey, secretkey);
			if (p == null)
				return "Provider is not registered!";
			VMType t = marketDAO.getVmType(type, p.getId());
			if (t == null)
				return "VM type is not registered!";
			marketDAO.removeVmType(t);
			return status_successfull;
		} catch (Exception e) {
			// logger.debug(e);
			e.printStackTrace();
			return status_failure;
		}
	}

	@RequestMapping(value = "/setavailables", method = RequestMethod.POST)
	public @ResponseBody
	String setAvailables(@RequestParam(value = "accesskey") String accesskey,
			@RequestParam(value = "secretkey") String secretkey,
			@RequestParam(value = "vmtype") String type,
			@RequestParam(value = "quantity") short quantity) {
		try {
			Provider p = marketDAO.getProvider(accesskey, secretkey);
			if (p == null)
				return "Provider is not registered";
			VMType vm = marketDAO.getVmType(type, p.getId());
			if (vm == null)
				return "VM Type is not registered";
			if (quantity < 1)
				return "quantity should be greater than zero";

			marketDAO.addAvailable(new Availables(p.getId(), vm.getId(),
					quantity, new Date()));

			// calling auction
			Prices price = runauction(p.getId(), vm.getId());
			marketDAO.addPrice(price);

			String output = "{\"price\":\"" + price.getPrice() + "\"}";

			return output;

		} catch (Exception e) {
			// logger.debug(e);
			e.printStackTrace();
			return status_failure;
		}
	}
	
	
	@RequestMapping(value = "/setmaxqty", method = RequestMethod.POST)
	public @ResponseBody
	String setMaxQty(@RequestParam(value = "accesskey") String accesskey,
			@RequestParam(value = "secretkey") String secretkey,
			@RequestParam(value = "vmtype") String type,
			@RequestParam(value = "quantity") short quantity) {
		try {
			Provider p = marketDAO.getProvider(accesskey, secretkey);
			if (p == null)
				return "Provider is not registered";
			VMType vm = marketDAO.getVmType(type, p.getId());
			if (vm == null)
				return "VM Type is not registered";
			if (quantity < 1)
				return "quantity should be greater than zero";

			marketDAO.addMaxQty(new MaxQuantity(p.getId(), vm.getId(), quantity, new Date()));

			return status_successfull;
		} catch (Exception e) {
			// logger.debug(e);
			e.printStackTrace();
			return status_failure;
		}
	}

	@RequestMapping(value = "/setreserveprice", method = RequestMethod.POST)
	public @ResponseBody
	String setReservePrice(@RequestParam(value = "accesskey") String accesskey,
			@RequestParam(value = "secretkey") String secretkey,
			@RequestParam(value = "vmtype") String type,
			@RequestParam(value = "value") double value) {
		try {

			Provider p = marketDAO.getProvider(accesskey, secretkey);
			if (p == null)
				return "Provider is not registered";
			VMType vm = marketDAO.getVmType(type, p.getId());
			if (vm == null)
				return "VM Type is not registered";
			if (value <= 0)
				return "Value should be greater than zero";
			marketDAO.addReservePrice(new ReservePrice(p.getId(), vm.getId(),
					value, new Date()));

			// calling auction
			Prices price = runauction(p.getId(), vm.getId());
			marketDAO.addPrice(price);

			String output = "{\"price\":\"" + price.getPrice() + "\"}";

			return output;

		} catch (Exception e) {
			// logger.debug(e);
			e.printStackTrace();
			return status_failure;
		}
	}

	
	@RequestMapping(value = "/setmaxprice", method = RequestMethod.POST)
	public @ResponseBody
	String setMaxPrice(@RequestParam(value = "accesskey") String accesskey,
			@RequestParam(value = "secretkey") String secretkey,
			@RequestParam(value = "vmtype") String type,
			@RequestParam(value = "value") double value) {
		try {

			Provider p = marketDAO.getProvider(accesskey, secretkey);
			if (p == null)
				return "Provider is not registered";
			VMType vm = marketDAO.getVmType(type, p.getId());
			if (vm == null)
				return "VM Type is not registered";
			if (value <= 0)
				return "Value should be greater than zero";
			
			marketDAO.addMaxPrice(new MaxPrice(p.getId(), vm.getId(), value, new Date()));

			return status_successfull;

		} catch (Exception e) {
			// logger.debug(e);
			e.printStackTrace();
			return status_failure;
		}
	}
	
	
	@RequestMapping(value = "/addbid", method = RequestMethod.POST)
	public @ResponseBody
	String addBid(@RequestParam(value = "accesskey") String accesskey,
			@RequestParam(value = "secretkey") String secretkey,
			@RequestParam(value = "vmtype") String type,
			@RequestParam(value = "quantity") short quantity,
			@RequestParam(value = "bid") double bid,
			@RequestParam(value = "ref") String ref) {

		try {
			Provider p = marketDAO.getProvider(accesskey, secretkey);
			if (p == null)
				return "Provider is not registered";
			VMType vm = marketDAO.getVmType(type, p.getId());
			if (vm == null)
				return "VM Type is not registered";
			if (quantity < 1)
				return "quantity should be greater than zero";
			if (bid < 0)
				return "bid should be a positive value";

			Bids b = marketDAO.getBid(p.getId(), ref);
			if (b != null)
				return "This reference number has been used previously!";
			
			double maxPrice = marketDAO.getRecentMaxPrice(p.getId(), vm.getId());
			if(maxPrice<bid)
				return "bid should be smaller than maximum bid price.";

			int maxQty = marketDAO.getRecentMaxQty(p.getId(), vm.getId());
			if(maxQty<quantity)
				return "Quantity should be smaller than maximum quantity.";

			
			b = new Bids(p.getId(), vm.getId(), quantity, bid, ref);
			marketDAO.addBid(b);

			// calling auction
			Prices price = runauction(p.getId(), vm.getId());
			marketDAO.addPrice(price);

			String output = "{\"price\":\"" + price.getPrice() + "\"}";

			return output;

		} catch (Exception e) {
			e.printStackTrace();
			return status_failure;
		}
	}

	@RequestMapping(value = "/updatebid", method = RequestMethod.POST)
	public @ResponseBody
	String updateBid(@RequestParam(value = "accesskey") String accesskey,
			@RequestParam(value = "secretkey") String secretkey,
			@RequestParam(value = "quantity") short quantity,
			@RequestParam(value = "ref") String ref) {

		try {
			Provider p = marketDAO.getProvider(accesskey, secretkey);
			if (p == null)
				return "Provider is not registered!";

			Bids b = marketDAO.getBid(p.getId(), ref);
			if (b == null)
				return "Wrong Reference Number!";
			
			int maxQty = marketDAO.getRecentMaxQty(p.getId(), b.getType());
			if(maxQty<quantity)
				return "Quantity should be smaller than maximum quantity.";

			marketDAO.uppdateBid(b, quantity);

			// calling auction
			Prices price = runauction(p.getId(), b.getType());
			marketDAO.addPrice(price);

			String output = "{\"price\":\"" + price.getPrice() + "\"}";

			return output;

		} catch (Exception e) {
			e.printStackTrace();
			return status_failure;
		}
	}

	@RequestMapping(value = "/removebid", method = RequestMethod.POST)
	public @ResponseBody
	String removeBid(@RequestParam(value = "accesskey") String accesskey,
			@RequestParam(value = "secretkey") String secretkey,
			@RequestParam(value = "ref") String ref) {

		try {
			Provider p = marketDAO.getProvider(accesskey, secretkey);
			if (p == null)
				return "Provider is not registered";

			Bids b = marketDAO.getBid(p.getId(), ref);
			if (b == null)
				return "Wrong Reference Number!";

			// calling auction
			Prices price = runauction(p.getId(), b.getType());
			marketDAO.addPrice(price);

			String output = "{\"price\":\"" + price.getPrice() + "\"}";

			return output;

		} catch (Exception e) {
			e.printStackTrace();
			return status_failure;
		}
	}

	
	@RequestMapping(value = "/pricehistory", method = RequestMethod.POST)
	public @ResponseBody
	String priceHistory(@RequestParam(value = "accesskey") String accesskey,
			@RequestParam(value = "secretkey") String secretkey,
			@RequestParam(value = "vmtype") String type,
			@RequestParam(value = "fromdate", required=false)  @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") Date fromDate,
			@RequestParam(value = "todate", required=false)  @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") Date toDate) {

		try {
			Provider p = marketDAO.getProvider(accesskey, secretkey);
			if (p == null)
				return "Provider is not registered";

			VMType vm = marketDAO.getVmType(type, p.getId());
			if (vm == null)
				return "VM Type is not registered";

			Date myDate = new Date(System.currentTimeMillis());
			if(fromDate == null)
				fromDate = new Date(myDate.getTime() - (10 * 24 * 60 * 60 * 1000)); //one day before
			
			if(toDate == null)
				toDate = new Date();

			
			
			List<Prices> ph = marketDAO.getAllPrices(p.getId(), vm.getId(), fromDate,  toDate); 

			String output = "{\"History\":[";
			for (Prices price : ph) {
				output += "{\"time\":\"" + price.getTimestamp() + "\"," + "\"price\":\"" + price.getPrice() + "\"},";
			}

			if(ph.size() != 0){
				return output.substring(0, output.length() - 1) + "]}";
			} else
				return output + "]}";

		} catch (Exception e) {
			e.printStackTrace();
			return status_failure;
		}
	}
	
	private Prices runauction(int provider, int vmtype) {
		double reservePrice = marketDAO.getRecentReservePrice(provider, vmtype);
		// System.out.println(reservePrice);

		int supply = marketDAO.getRecentSupply(provider, vmtype);
		// System.out.println(supply);

		// bidlist must be sorted based on the bid value
		List<Bids> bidlist = marketDAO.getAllBids(provider, vmtype);

		Prices lastprice = marketDAO.getPrice(provider, vmtype);

		// System.out.println(lastprice.toString());

		Prices price = AuctionWrapper.getprice(provider, vmtype, bidlist,
				reservePrice, supply, lastprice);
		// System.out.println(price.toString());



		return price;
	}

}
