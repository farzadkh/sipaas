package org.cloudbus.MarketCloud;

import org.cloudbus.MarketCloud.DAO.MarketDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/customerservices")
public class CustomerController {
//	private String status_successfull = "{\"status\":\"success\"}";
//	private String status_failure = "{\"status\":\"fail\"}";

	// private Logger logger = Logger.getLogger("AuctionController");

	@Autowired
	private MarketDAO marketDAO;

//	@RequestMapping(value = "/getproviders", method = RequestMethod.GET)
//	public @ResponseBody
//	String getProvidersList() {
//
//		List<Provider> providers = marketDAO.getAllProviders();
//		String output = "{\"Providers\":[";
//		for (Provider p : providers) {
//			output += "{\"name\":\"" + p.getName() + "\"},";
//		}
//		return output.substring(0, output.length() - 1) + "]}";
//	}

//	@RequestMapping(value = "/getvmtypes", method = RequestMethod.GET)
//	public @ResponseBody
//	String getavailableVMTypes(@RequestParam(value = "provider") String provider) {
//		try {
//			Provider p = marketDAO.getProvider(provider);
//			if (p == null)
//				return "Provider is not registered";
//
//			List<VMType> vmTypes = marketDAO.getAllVMs(p.getId());
//
//			String output = "{\"VMTypes\":[";
//			for (VMType vmType : vmTypes) {
//				output += "{\"name\":\"" + vmType.getType() + "\"},";
//			}
//
//			if(vmTypes.size() != 0){
//				return output.substring(0, output.length() - 1) + "]}";
//			} else
//				return output + "]}";		
//			
//		} catch (Exception e) {
//			e.printStackTrace();
//			return status_failure;
//		}
//	}

//	@RequestMapping(value = "/setbid", method = RequestMethod.POST)
//	public @ResponseBody
//	String setBidForVM(@RequestParam(value = "provider") String provider,
//			@RequestParam(value = "username") String username,
//			@RequestParam(value = "password") String password,
//			@RequestParam(value = "vmtype") String type,
//			@RequestParam(value = "quantity") short quantity,
//			@RequestParam(value = "bid") double bid) {
//		try {
//			Provider p = marketDAO.getProvider(provider);
//			if (p == null)
//				return "Provider is not registered";
//			VMType vm = marketDAO.getVmType(type, p.getId());
//			if (vm == null)
//				return "VM Type is not registered";
//			if (quantity < 1)
//				return "quantity should be greater than zero";
//			
//			ComputeService cs = CloudProviderFactory.getProviderInstance(
//					provider, username, password);
//
//			if (cs != null) {
//
//				marketDAO.addBid(new Bids(p.getId(), vm.getId(), quantity, bid));
//				List<Bids> bidList = marketDAO.listBids(p.getId(), vm.getId());
//
//				// DBOperations dbOp = new DBOperations();
//				// if(dbOp.insertBid(providerName.toLowerCase(),username,VmTypeName,numberofRequestedVMs,bid)){
//				// Auctions auction = Auctions.getAuction(providerName,
//				// VmTypeName);
//				// auction.runAuction(providerName, VmTypeName,username,cs);
//
//				return status_successfull;
//
//			} else {
//				return "Authenticataion failed!";
//			}
//		} catch (Exception e) {
//			e.printStackTrace();
//			return status_failure;
//		}
//	}

}
