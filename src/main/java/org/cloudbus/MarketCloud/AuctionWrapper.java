package org.cloudbus.MarketCloud;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import org.cloudbus.MarketCloud.DAO.Bids;
import org.cloudbus.MarketCloud.DAO.Prices;
import org.cloudbus.auction.AuctionAbstract;
import org.cloudbus.auction.AuctionConsensusRevenueExtract;
import org.cloudbus.auction.Order;

public class AuctionWrapper {
	public static final long precision = 100000;
	public static Prices getprice(int provider, int vmtype, List<Bids> list, double resprice, int supply, Prices lastprice){
		long minimumIgnoredBidPrice = 0;
		if (lastprice == null){
			lastprice = new Prices(provider, vmtype, new Date(), 0, 0);
		}
		long previousOptimalPrice =  (long) (lastprice.getOptimalprice() * precision);
		long seed = new Random().nextLong();
		long reserveprice = (long) (resprice * precision);
		
		ArrayList<Order> orderlist = new ArrayList<Order>();
		int  remainedsupply = supply;
		int i = 0;
		for(Bids bid:list)
		{
			long value = (long) (bid.getBid() * precision);
			if(value < reserveprice){
			  minimumIgnoredBidPrice = value;
			  break;
			}
				
			int qty = bid.getQty();
			if( remainedsupply > qty){
				Order order = new Order(1, value, qty);
				orderlist.add(order);
				i++;
			}
			else if (remainedsupply == qty){
				orderlist.add(new Order(1, value, qty));
				if(i+1 < list.size())
				  minimumIgnoredBidPrice = (long) (list.get(i+1).getBid() * precision);
				break;
			}
			else {
				orderlist.add(new Order(1, value, remainedsupply));
				minimumIgnoredBidPrice = (long) (bid.getBid() * precision);
				break;
			}
			remainedsupply -= qty;
		}
		
		
		if (supply==0){
			return new Prices(provider, vmtype, new Date(), Double.MAX_VALUE, lastprice.getOptimalprice());
		}
		
		System.out.println(" minimumIgnoredBidPrice=" + minimumIgnoredBidPrice);
		System.out.println(" previousOptimalPrice=" + previousOptimalPrice);

		AuctionAbstract auction = new AuctionConsensusRevenueExtract(orderlist, seed);
		long optPrice = auction.getOptimalSinglePrice();
		long p;
		if(previousOptimalPrice == optPrice){
			p =  Math.max((long) (lastprice.
					getPrice() * precision),  Math.max(minimumIgnoredBidPrice,reserveprice));
			return new Prices(provider, vmtype, new Date(), p/(double) precision, lastprice.getOptimalprice());
			
		}
		else
			previousOptimalPrice = optPrice;
		
		double pp = Math.max(auction.getSinglePrice() / (double) precision, Math.max(minimumIgnoredBidPrice,reserveprice));
		return new Prices(provider, vmtype, new Date(), pp, optPrice/(double) precision);
	}
	
}
