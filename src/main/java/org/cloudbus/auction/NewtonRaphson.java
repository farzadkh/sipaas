package org.cloudbus.auction;

/**
 * 
 * This class calculate the value c in ln c = c / p - 1 using the Newton-Raphson method.
 * f(c) = (1/ln(c)) . (1/p - 1/c) => df(c)/dc = p ln(c) + p - c / (p c^2 (log c)^2)
 * We are intesretd in a value of c that maximize the f(c).
 * => c<>0 and c<> 1,  p ln(c) + p - c = 0
 * We solve this through numerical method.  
 * It stops when the approximation is less than 0.00005
 * 
 * 
 * @author Adel Nadjaran Toosi
 *
 */


public class NewtonRaphson {
    public static double solve(double p) 
    {

    //trying to find good initial value for x0;
    int c;
    for(c = (int)p; p * Math.log(c) > c - p; c++);
    //c--;
    	
   	double xn = c;
    double xn1, f, f1;
 	
    while (true)
    {
    	f = p * Math.log(xn) + p - xn;
    	f1 = p / xn - 1;
        xn1 = xn - f / f1;
        
        if (Math.abs(xn1 - xn)<= 0.00005) 
        	break;
        
        xn = xn1;
    }
    
    return xn1;
    }

}
