package org.cloudbus.auction;

import java.util.ArrayList;

public abstract class AuctionAbstract implements IAuction{
	
	// list of current running instances including bids must be sorted based on the bid price
	protected ArrayList<Order> sortedList;
	protected int maxqty;
	
	public AuctionAbstract(ArrayList<Order> list) {
		
		int maxr = 0;
		
		Order prv = null;
		if(!list.isEmpty()){
			prv = list.get(0);
			maxr = prv.getQuantity();
		}
		
		for (Order order : list) {
			
			if(order.getQuantity() > maxr)
				maxr = order.getQuantity();
			
			if(prv.getBidPrice() < order.getBidPrice())
				throw new RuntimeException("List of orders is not sorted");
			
		prv = order;
		}
		
		sortedList = list;
		maxqty = maxr;

	}
	
	/**
	 * 
	 * returns single price to uniformly charge all orders.
	 * 
	 */
	public abstract long getSinglePrice();
	
	
	/**
	 * 
	 * calculates optimal single price w.r.t. order vector orders.
	 * 
	 * @param orders
	 *            order vectors
	 * @return price
	 */
	public long getOptimalSinglePrice(ArrayList<Order> orders) {
		Order lastwiningins = null;
		long max = 0;
		int size = orders.size();

		if (!orders.isEmpty()) {
			int sumSize = getNumberofItems(orders);
			long revenueofLastOrder = orders.get(size - 1).getBidPrice() * sumSize;
			max = revenueofLastOrder;
			lastwiningins = orders.get(size - 1);

			for (int i = size - 1; i >= 0; i--) {
				long revenue = orders.get(i).getBidPrice() * sumSize;
				sumSize -= orders.get(i).getQuantity();
				// System.out.println(revenue);
				if (revenue >= max) {
					max = revenue;
					lastwiningins = orders.get(i);
				}
			}
		}

		if (lastwiningins != null)
			return lastwiningins.getBidPrice();
		else
			return 0;

	}
	
	
	public int getNumberofItems(ArrayList<Order> orders){
		int k=0;
		for(Order order:orders){
			k += order.getQuantity();
		}
		return k;
	}
	
	/**
	* 
	* claculates optimal single price w.r.t. internal order vector orders.
	* 
	* @param orders
	*            order vectors
	* @return price
	*/
	public long getOptimalSinglePrice() {
		return getOptimalSinglePrice(sortedList);
	}


	public ArrayList<Order> getSortedList() {
		return sortedList;
	}

}
