package org.cloudbus.MarketCloud;

public class VMRequest {
	private double bid;
	private String type;
	//private String provider;
	private int number;
	
	
	public int getNumber() {
		return number;
	}
	public void setNumber(int number) {
		this.number = number;
	}
	public double getBid() {
		return bid;
	}
	public void setBid(double bid) {
		this.bid = bid;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	/*public String getProvider() {
		return provider;
	}
	public void setProvider(String provider) {
		this.provider = provider;
	}*/

}
