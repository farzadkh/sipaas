package org.cloudbus.MarketCloud;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;


public class DBOperations {
	
	private static final String DB_USER = "root";
	private static final String DB_PASS = "rootfery";
	private static final String DB_URL = "localhost";
	
	private Connection connect = null;
	private Statement statement = null;
	private PreparedStatement preparedStatement = null;
	private ResultSet resultSet = null;
	public  Connection getConnection(){
		try{
			Class.forName("com.mysql.jdbc.Driver");
			Connection connect = DriverManager
			          .getConnection("jdbc:mysql://"+DB_URL+"/auctionDB?"
			              + "user="+DB_USER+"&password="+DB_PASS);
			//this.connect = connect;
			
			return connect;
		}
		catch(Exception e){
			System.out.println(e);
			return null;
		}
		
	}
	public boolean insertBid(String providerName, String username,
			/*String password,*/ String vmTypeName, int numberofRequestedVMs,
			double bid){
		try {
		      // this will load the MySQL driver, each DB has its own driver
		      //Class.forName("com.mysql.jdbc.Driver");
		      // setup the connection with the DB.
		      //connect = DriverManager
		      //    .getConnection("jdbc:mysql://"+DB_URL+"/auctionDB?"
		      //        + "user="+DB_USER+"&password="+DB_PASS);
			if(connect == null)
				this.connect = getConnection();
		      // statements allow to issue SQL queries to the database
		      statement = connect.createStatement();
		      // resultSet gets the result of the SQL query
		      /*resultSet = statement
		          .executeQuery("select * from FEEDBACK.COMMENTS");
		      writeResultSet(resultSet);
*/
		      // preparedStatements can use variables and are more efficient
		      preparedStatement = connect
		          .prepareStatement("insert into  auctionDB.bidTbl values (default, ?, ?, ? , ?, ?)");
		      // "myuser, webpage, datum, summary, COMMENTS from FEEDBACK.COMMENTS");
		      // parameters start with 1
		      preparedStatement.setString(1, providerName);
		      preparedStatement.setString(2, username);
		      //preparedStatement.setString(3, password);
		      preparedStatement.setString(3, vmTypeName);
		      preparedStatement.setInt(4, numberofRequestedVMs);
		      preparedStatement.setDouble(5, bid);
		      preparedStatement.executeUpdate();
		      
		      /*preparedStatement = connect
		          .prepareStatement("SELECT myuser, webpage, datum, summary, COMMENTS from FEEDBACK.COMMENTS");
		      resultSet = preparedStatement.executeQuery();
		      writeResultSet(resultSet);

		      // remove again the insert comment
		      preparedStatement = connect
		      .prepareStatement("delete from FEEDBACK.COMMENTS where myuser= ? ; ");
		      preparedStatement.setString(1, "Test");
		      preparedStatement.executeUpdate();
		      
		      resultSet = statement
		      .executeQuery("select * from FEEDBACK.COMMENTS");
		      writeMetaData(resultSet);*/
		      return true;
		    } catch (Exception e) {
		      return false;
		    } finally {
		      close();
		    }

		
	}
	public ArrayList<VMRequest> readBids(String providerName, String username,String vmType){
		
		try {
			if(connect!=null)
				getConnection();
			statement = connect.createStatement();
			preparedStatement = connect.prepareStatement("SELECT * from bidTbl "
					+ "where provider=? AND username=? AND vmType=?");
			preparedStatement.setString(1, providerName);
			preparedStatement.setString(2, username);
			preparedStatement.setString(3, vmType);
			resultSet = preparedStatement.executeQuery();
			ArrayList<VMRequest> vmrequest = new ArrayList<VMRequest>();
			while(resultSet.next()){
				VMRequest vm = new VMRequest();
				vm.setBid(resultSet.getDouble("bid"));
				vm.setNumber(resultSet.getInt("count"));
				vmrequest.add(vm);
				
			}
			return vmrequest;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
		
		
	}
	private void close() {
		
		  try {
			  if(resultSet != null)
				  resultSet.close();
			statement.close();
			connect.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		  
		}
	

}
