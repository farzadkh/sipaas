package org.cloudbus.MarketCloud.DAO;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="bids")
public class Bids {
	public Bids() {
		// TODO Auto-generated constructor stub
	}

	public Bids(int provider, int vmtype, short qty, double bid, String ref) {
		super();
		this.provider = provider;
		this.type = vmtype;
		this.qty = qty;
		this.bid = bid;
		this.bidref = ref;
	}

	
	@Id
	@GeneratedValue
	private int id;

	private int provider;
	
	@Column(name="type")
	private int type ;
	
	private short qty;
	
	private double bid;
	
	private String bidref;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getProvider() {
		return provider;
	}

	public void setProvider(int provider) {
		this.provider = provider;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public short getQty() {
		return qty;
	}

	public void setQty(short qty) {
		this.qty = qty;
	}

	public double getBid() {
		return bid;
	}

	public void setBid(double bid) {
		this.bid = bid;
	}

	public String getBidref() {
		return bidref;
	}

	public void setBidref(String bidref) {
		this.bidref = bidref;
	}
	
}
