package org.cloudbus.auction;

import java.util.ArrayList;
import java.util.Random;

public class AuctionConsensusRevenueExtract extends AuctionAbstract implements IAuction{
	Random random;

	public AuctionConsensusRevenueExtract(ArrayList<Order> list, long seed) {
		super(list);
		random = new Random(seed);
	}

	public AuctionConsensusRevenueExtract(ArrayList<Order> list, Random rng) {
		super(list);
		random = rng;
	}

	
	/**
	 * 
	 * Calculates optimal single price w.r.t. order vector orders independent of the order i, i. e., 
	 * order i has been removed. 
	 * 
	 * @param orders
	 *            order vectors
	 * @return price
	 */	
	public long getOptimalSinglePriceWithout(int i) {
		return getOptimalSinglePrice(getOrdersIndependent(i));
	}

	
	/**
	 * 
	 * claculates optimal revenue w.r.t. order vector orders.
	 * 
	 * @param orders
	 *            order vectors
	 * @return max revenue
	 */
	private long getOptimalRevenue(ArrayList<Order> orders) {
		Order lastwiningins = null;
		long max = 0;
		int size = orders.size();

//		if (supply == 0)
//			return Long.MAX_VALUE;

		if (!orders.isEmpty()) {
			int sumSize = getNumberofItems(orders);
			long revenueofLastOrder = orders.get(size - 1).getBidPrice()
					* sumSize;
			max = revenueofLastOrder;
			lastwiningins = orders.get(size - 1);

			for (int i = size - 1; i >= 0; i--) {
				long revenue = orders.get(i).getBidPrice() * sumSize;
				sumSize -= orders.get(i).getQuantity();
				// System.out.println(revenue);
				if (revenue >= max) {
					max = revenue;
					lastwiningins = orders.get(i);
				}
			}
		}

		if (lastwiningins != null)
			return max;
		else
			return 0;

	}

	public long getOptimalRevenue() {
		return getOptimalRevenue(sortedList);
	}

	public long getOptimalRevenue(int i) {
		return getOptimalRevenue(getOrdersIndependent(i));
	}

	/**
	 * 
	 * creates orders vector without ith element
	 * 
	 * @param orders
	 *            orders vector
	 * @param i
	 *            ith element
	 * @return orders vector without ith element
	 */
	public ArrayList<Order> getOrdersIndependent(int i) {
		ArrayList<Order> temp = new ArrayList<Order>(sortedList);
		temp.remove(sortedList.get(i));
		return temp;
	}

	/**
	 * 
	 * Given order vector, ﬁnd the largest k such that the highest k bidders can
	 * equally share the revenue. Charge each of these bidders revenue/k. If no
	 * k exists return -1.
	 * 
	 * @param revenue
	 * @return largest k that can share the revenue
	 */
	public int getRevenueExtract(ArrayList<Order> list, double revenue) {

		int sum = getNumberofItems(list);
		for (int i = list.size() - 1; i >= 0; i--) {
			Order order = list.get(i);


			if (((double) revenue / (sum)) <= (double) order.getBidPrice())
				return sum;	

			sum -= order.getQuantity();

		}
		return -1;

	}

	
	public int getRevenueExtract(double revenue) {
		return getRevenueExtract(this.sortedList, revenue);
	}
	
	
//	public int getRevenueExtract2(InstanceGroupSortedList list, double revenue) {
//
//		int sum = 0;//list.getNumberofInstances() - getPartial();
//		for (int i = 0; i<=list.size() - 1; i++) {
//			InstanceGroup insgrp = list.get(i);
//			if (i == list.size() - 1)
//				sum += insgrp.size() - getPartial();
//			else
//				sum += insgrp.size();
//			
//			if (((double) revenue / sum) <= (double) insgrp.getBidPrice()){
//				//while(((double) revenue / sum) <= (double) insgrp.getBidPrice()) sum--;
//				return sum;
//			}
//		}
//		return -1;
//	}
	
	
//	public int getRevenueExtract3(InstanceGroupSortedList list, double revenue) {
//
//		int sum = 0;//list.getNumberofInstances() - getPartial();
//		for (int i = 0; i<=list.size() - 1; i++) {
//			InstanceGroup insgrp = list.get(i);
//			if (i == list.size() - 1)
//				sum += insgrp.size() - getPartial();
//			else
//				sum += insgrp.size();
//			
//			if (((double) revenue / sum) <= (double) insgrp.getBidPrice()){
//				while(((double) revenue / sum) <= (double) insgrp.getBidPrice()) sum--;
//				return sum;
//			}
//		}
//		return -1;
//	}
	
	
//	public int getRevenueExtract2(double revenue) {
//		return getRevenueExtract2(this.sortedList, revenue);
//	}
//	
//	public int getRevenueExtract3(double revenue) {
//		return getRevenueExtract3(this.sortedList, revenue);
//	}
//	
	

	/**
	 * 
	 * calculates nearest nearest c^(i+U) for integer i to optimal revenue.
	 * 
	 * @param c
	 *            a constant
	 * @return nearest c^(i+U), U is random varialble uniformly distributed in
	 *         [0,1]
	 */
	public double r(ArrayList<Order> list, double c, double U) {
		// U is s uniform number in [0,1]
		// Random random = new Random();

		// SEED do not forget.....
		// let function optimalRevenue(·) rounded down to nearest c^(i+U) for
		// integer i.
		long optimalRevenue = getOptimalRevenue(list);
		int i = (int) Math.floor((Math.log(optimalRevenue) / Math.log(c) - U));

		return Math.pow(c, i + U);

	}

	public double r(double c, double U) {
		return r(sortedList, c, U);
	}
	
	
	/**
	* 
	* Calculate revenue if single sale price is price
	* 
	* @param list order vector
	* @param price single sale price
	* @return revenue
	*/
	public double getRevenueBySinglePrice(ArrayList<Order> list, double price){
		int sum = 0;
		for(Order order:list){
			if(price > order.getBidPrice())
				break;
			sum +=order.getQuantity();
		}
		return sum * price;
	}

	
	public double getRevenueBySinglePrice(double price){
		return getRevenueBySinglePrice(this.sortedList, price);
	}
	
	public int numberItemSoldByOptimalAuction(){
		long price = getOptimalSinglePrice();
		int m, l = 0;
		int sum=0;
		for (l = sortedList.size()-1; l >= 0; l--) {
			Order insgroup = sortedList.get(l); 
			if (price == insgroup.getBidPrice()) {
				break;
			}
			sum = sum + insgroup.getQuantity();
		}
		
		m = getNumberofItems(sortedList) - sum;
		return m;
	}
	
	@Override
	public String toString() {
		return sortedList.toString();
	}


	@Override
	public long getSinglePrice() {
		int m = numberItemSoldByOptimalAuction();
		// Think more why returning optimal in very rare case that 
		// m is small and maxr is big we return optimal price
		if(m <= maxqty){
			return getOptimalSinglePrice();
		}
		double rho = (double)m / (m - maxqty);
		double c = NewtonRaphson.solve(rho);
		double U = random.nextDouble();
		double rb = r(c, U);
		int x = getRevenueExtract(rb);
		double cs_r = rb / x;
		return (long) Math.floor(cs_r);
		
	}

}
