package org.cloudbus.MarketCloud.DAO;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="maxprice")
public class MaxPrice {
	
	public MaxPrice() {
		// TODO Auto-generated constructor stub
	}		
	
	public MaxPrice(int provider, int type, double price, Date timestamp) {
		super();
		this.provider = provider;
		this.type = type;
		this.price = price;
		this.timestamp = timestamp;
	}

	@Id
	@GeneratedValue
	private int id;
	
	private int provider;
	
	@Column(name="vmtype")
	private int type ;
	
	private double price;
	
	@Column(name="time")
	private Date timestamp;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getProvider() {
		return provider;
	}

	public void setProvider(int provider) {
		this.provider = provider;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}
	
}
